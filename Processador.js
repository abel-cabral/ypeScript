"use strict";
exports.__esModule = true;
var Processador = /** @class */ (function () {
    function Processador(marca, modelo, frequencia) {
        this.marca = marca;
        this.modelo = modelo;
        this.frequencia = frequencia;
    }
    return Processador;
}());
exports.Processador = Processador;
