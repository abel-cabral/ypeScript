"use strict";
exports.__esModule = true;
var Processador_1 = require("./Processador");
var Video_1 = require("./Video");
var Computador = /** @class */ (function () {
    function Computador(qtd_hd, qtd_memoria, monitor_size, mae, proc, video) {
        this.qtd_hd = qtd_hd;
        this.qtd_memoria = qtd_memoria;
        this.monitor_size = monitor_size;
        this.mae = mae;
        this.proc = proc;
        this.video = video;
    }
    return Computador;
}());
exports.Computador = Computador;
var proc = new Processador_1.Processador('AMD', 'Ryzen 5', '3.1 Ghz');
var video = new Video_1.Video('Nvidia', 'GTX 1060', 6);
var computador = new Computador('1TB', '16GB', 'LED 29"', 'Asus AM4', proc, video);
console.log(computador);
