import {Processador} from './Processador';
import {Video} from './Video';

export class Computador{
    private qtd_hd: string;
    private qtd_memoria: string;
    private monitor_size: string;
    private mae: string;
    private proc: object;
    private video: object;

    constructor(qtd_hd: string, qtd_memoria: string, monitor_size: string, mae: string, 
        proc: object, video: object){
        this.qtd_hd = qtd_hd;
        this.qtd_memoria = qtd_memoria;
        this.monitor_size = monitor_size;
        this.mae = mae;
        this.proc = proc;
        this.video = video;
    }
}

let proc = new Processador('AMD', 'Ryzen 5', '3.1 Ghz');
let video = new Video('Nvidia', 'GTX 1060', 6);
let computador = new Computador('1TB', '16GB', 'LED 29"', 'Asus AM4', proc, video);

console.log(computador)
