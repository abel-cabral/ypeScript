"use strict";
exports.__esModule = true;
var Video = /** @class */ (function () {
    function Video(marca, modelo, qtd_memoria) {
        this.marca = marca;
        this.modelo = modelo;
        this.qtd_memoria = qtd_memoria;
    }
    return Video;
}());
exports.Video = Video;
