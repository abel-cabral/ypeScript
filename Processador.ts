export class Processador{
    private marca: string;
    private modelo: string;
    private frequencia: string;

    constructor(marca: string, modelo: string, frequencia: string){
        this.marca = marca;
        this.modelo = modelo;
        this.frequencia = frequencia;
    }

}
