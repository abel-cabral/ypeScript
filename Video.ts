export class Video{
    private qtd_memoria: number;
    private marca: string;
    private modelo: string;    

    constructor(marca: string, modelo: string, qtd_memoria: number){
        this.marca = marca;
        this.modelo = modelo;
        this.qtd_memoria = qtd_memoria;
    }
} 